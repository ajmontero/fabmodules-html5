# Fab Modules Project

Fab Modules is a browser-based CAM system, which allows to generate toolpaths for and control
lasercutters, CNC-mills and waterjets commonly found in [fablabs](https://www.fablabs.io/).

![Milling circuit boards](./screenshot-pcbmilling.png)

## Supported machines

Lasercutters

* Epilog
* Trotec
* GCC
* Universal

Vinylcutters

* GCC
* Roland

CNC-mills

* Shopbot
* Roland MDX-15/20/40 and SRM-20
* Smoothieboard
* Othermill

Speciality machines

* Oxford ulaser
* Resonetics Excimer microlaser
* Omax waterjet

Other tools

* Generic G-code output
* Gerber file
* DXF and SVG output
* Mesh

## Instalación en Ubuntu

### Prerequisitos

* git  
  ```bash
  sudo apt install git
  ```

* curl  
  ```bash
  sudo apt install curl
  ```
* node.js & npm  
  ```bash
  sudo apt install nodejs npm
  ```
* Python  
  ```bash
  sudo apt install python
  ```
* pip  
  ```bash
  curl https://bootstrap.pypa.io/get-pip.py --output get-pip.py  
  sudo python2 get-pip.py  
  rm get-pip.py
  ```
* PySerial  
  ```bash
  sudo pip install pyserial
  ```
* Python TKinter  
  ```bash
  sudo apt install python-tk
  ```

### Instalación

* Ir a la carpeta de usuario  
  ```bash
  cd
  ```
* Clonar fabmodules  
  ```bash
  git clone https://github.com/FabModules/fabmodules-html5.git fabmodules
  ```  

  Hay, por si acaso, otra copia de 2/nov/2020 aquí, en  
  ```bash
  git clone https://gitlab.com/ajmontero/fabmodules-html5.git fabmodules
  ```
* Ir a la carpeta de fabmodules  
  ```bash
  cd fabmodules
  ```
* Instalar los módulos  
  ```bash
  npm install
  ```
* Darse permisos a uno mismo para el USB/Serie  
  ```bash
  sudo adduser $(whoami) dialout
  ```   

### Uso con Roland MDX-20

#### Cositas previas

No seas tacaño con el cobre. Si vas a hacer el corte de la placa el diseño deberá estar colocado en un sitio en el que la fresadora con su fresa pueda comer material, sino no sabrá qué hacer. Es decir, que si vas a cortar con una fresa de 1 mm, en la imagen png, entre la línea de corte y el borde de la imagen deberá haber como mínimo 1mm , desde luego es mejor 2 mm, para asegurarse.  
Las pistas (que deberían ser de 0,3 -> 0,4 mm de grueso, aunque yo las he hecho de 0,2mm, pero pueden saltar; preveerlo en kicad) es mejor trazarlas con la fresa de 0,4 (intervías de 0,4; preveerlo en kicad).  
Los orificios, mejor con fresa de 0,6 mm (preveerlo en kicad).  
En kicad -> gerber -> svg salvamos tres capas, pistas, agujeros y corte externo de la pcb. Las metemos como capas en Gimp (resolución 1000x1000 y revisamos todo, incluido lo de invertir la capa (simetría horizontal) y los colores, ya que la fresa atacará el color negro y dejará el blanco. Cada capa se guarda como png.  
En 1000x1000ppp redondeando hacia arriba
    - 0,4mm = 16px  
    - 0,6mm = 24px  
    - 1,0mm = 39px  
    - 1,5mm = 59px  
    - 2,0mm = 79px  

#### Conexión de cables

Cuando conectamos el conversor Serie a USB, lo suyo es que abramos una terminal y hagamos `dmesg` para asegurarnos de que se ha conectado bien y el puerto en el que está, típicamente `/dev/ttyUSB0`. Puede que haga falta cambiarle los permisos usando `sudo chmod 777 /dev/ttyUSB0`  

#### Preparar la fresadora

Hay que poner una cama mártir para no estropear la base ni las fresas. La mía es de metacrilato.  
El (x:0, y:0) de la máquina está en la esquina inferior izquierda de la cuadrícula de la base. 
El (z:0) está en el punto en el que la dejemos con los botones UP/DOWN del frontal de la máquina. Si se apaga la máquina, cuando se vuelva a encender el z=0 estará en el sitio en el que estuviera al apagarla.  
Para ajustar el z:0,
* Colocar el cabezal en algún lugar encima de la placa de cobre, lo suficientemente alta como para meter la fresa.
* Quitar la tapa.
* Meter la fresa todo lo que podamos, ajustando uno de los tornillos lo suficiente como para que no caiga la fresa.
* Poner la tapa.
* Con los botones UP/DOWN de la máquina ajustar la altura como para que sobresalga el caño de la fresa entre 1 y 1,5 cm.
* Quitar la tapa.
* Aflojar el tornillo para que caiga la fresa sobre el cobre.
* Apretar los dos tornillos, estándo en contacto la fresa con el cobre. Este será el nuevo _z:0_.

#### Preparar el software
Seguir las instrucciones del repositorio, sabiendo que:
* Abrir un terminal y  
  ```bash
  cd ~/fabmodules  
  npm start
  ```   
* Dejar abierto el terminal para ver los mensajes.  
* Abrir el navegador en [http://localhost:12345](http://localhost:12345).  
* Cuando cargamos una imagen, elegimos la salida (Roland mill(\*.rml) y nos salen esos botones de "move to xy0" (El punto en el que está la esquina inferior izquierda de la placa de cobre. Es el origen de coordenadas.), "move to xy0 zjog" (Es decir el x:0 y:0 z:zjog; zjog es la altura de seguridad de la fresa en los desplazamientos) y "move to xyzhome and stop" (es decir, el sitio de la placa de cobre donde queremos que empieze a trabajar. Relativo al origen de coordenadas), no perdáis el tiempo dándole, porque no funcionará hasta que no seleccioneis el tipo de trabajo que se va a realizar ("PCB Traces,..."):
* Cuando seleccionamos el tipo de trabajo a realizar y nos sale el cuadro de texto "send command:", debería poner `mod_serial.py /dev/ttyUSB0 9600 dsrdtr`, si es que el puerto USB es `/dev/ttyUSB0`. Si no lo pone, lo escribes. A partir de entonces si le das a los botones que he dicho antes de "moveto ..." debería funcionar.
* Las pruebas, "con gaseosa". Es decir, hay que habituarse a la máquina sin una fresa instalada hasta manejarla con comodidad.

See [the wiki](https://github.com/FabModules/fabmodules-html5/wiki) for further instructions.

## License

Open source, [custom MIT-like license](./LICENSE.md)
